# pramdisk

This project implements a simple in-memory bio-based block device for linux ~6.9.1 (no compatibility guarantees, or any at all tbh).

### Building

In order to build it, make sure you have linux headers installed and run:
```sh
   make
```
### Loading
Then, you can load it with:
```sh
   insmod src/pramdisk.ko <pramdisk_major=<int>> <hardsect_size=<int>> <nsectors=<int>> <ndevices=<int>>
```

### Parameters

The arguments are the following:
- `pramdisk_major`: lets you choose the device major number, automatically selects one for you by default.
- `hardsect_size`: lets you choose the "hardware" sector size. By default the kernel sector size of 512 (maybe broken)
- `nsectors`: lets you change the size of the drive. Each sector is `hardsect_size` big. By default 1024 (512 Kb).
- `ndevices`: lets you choose to create several devices at once. By default, 1.

### Unloading

You can unload it with:
```sh
   rmmod pramdisk
```

### Make a filesystem and go!

Now that it's loaded, you'll get `ndevices` number of devices in `/dev/`, formatted as `/dev/pramdisk_<nb>`.

You can see the content of the disk by reading `/dev/pramdisk_<nb>`, but I recommend using a hex viewer like `hexdump` or `xxd` to easily see all the bytes.
The disk is zeroed out at the beginning.

You can now create a filesystem on it with for example:
```
mkfs.ext4 /dev/pramdisk_<nb>
```
for an `ext4` file system.

Note: this is a very nice way of seeing how different filesystems interact with the bytes of your drive.
Pretty neat huh?

You can zero out the bytes of the drive with:
```
dd if=/dev/zero of=/dev/pramdisk_<nb> bs=<block size> count=<nsectors>
```
Note: it makes more sense that `dd` operates in terms of blocks after making this project, it was simply originally meant to copy data to drives (which are segmented in blocks)!

Once you have a filesystem on the drive, you can mount it into your filesystem like any other drive:
```
mount /dev/pramdisk_<nb> <path/to/mountpoint>
```

Now, you can do any filesystem-level logic on the drive: create files, directories, manage permissions, etc... on the drive!
