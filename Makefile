ifneq ($(KERNELRELEASE),)

obj-m := src/pramdisk.o

else

KERNELDIR ?= /lib/modules/$(shell uname -r)/build
PWD := $(shell pwd)

default:
	$(MAKE) -C $(KERNELDIR) M=$(PWD) modules

endif

.PHONY: clean
clean:
	rm -rf src/*.o src/*~ src/core src/.depend .*.cmd src/.*.cmd src/*.ko src/*.mod.c .tmp_versions src/*.mod *.mod modules.order *.symvers
