#include <linux/version.h> 	/* LINUX_VERSION_CODE  */

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>

#include <linux/vmalloc.h>
#include <linux/kernel.h> /* printk */
#include <linux/blk-mq.h>
#include <linux/blkdev.h>
#include <linux/slab.h>		/* kmalloc() */
#include <linux/types.h>	/* size_t */
#include <linux/bio.h>
#include <linux/fs.h>		/* everything... */
#include <linux/errno.h>	/* error codes */
#include <linux/timer.h>
#include <linux/sched.h>
#include <linux/buffer_head.h>	/* invalidate_bdev */
#include <linux/hdreg.h> /*HDIO_ stuff*/


#define INVALIDATE_DELAY 50000
#define PRAMDISK_MINORS 16

struct pramdisk_dev {
   sector_t size_in_sectors; //nb_sectors
   u8* data; //Data array
   struct gendisk* gd;
};

static int pramdisk_major = 0;
module_param(pramdisk_major, int, 0);
static int hardsect_size = 512;
module_param(hardsect_size, int, 0);
static int nsectors = 1024;
module_param(nsectors, int, 0);
static int ndevices = 1;
module_param(ndevices, int, 0);


static struct pramdisk_dev* devs = NULL;


static int pramdisk_open(struct gendisk* gd, fmode_t mode) {
   /*struct pramdisk_dev* dev = gd->private_data;*/
   printk(KERN_INFO "pramdisk_open\n");

   return 0;
}


static void pramdisk_release(struct gendisk* gd) {
   /*struct pramdisk_dev* dev = gd->private_data;*/
   printk(KERN_INFO "pramdisk_release\n");
}

static int pramdisk_ioctl(struct block_device *bdev, blk_mode_t mode, unsigned int cmd, unsigned long arg) {
   struct pramdisk_dev* dev = bdev->bd_disk->private_data;
   struct hd_geometry geo;
   long size;

   switch(cmd) {
      //This ioctl gets the "disk geometry"
      //We're a ramdisk, so we don't really have one
      //It breaks a bunch of tools not to specify those so we do
      case HDIO_GETGEO: {
         size = dev->size_in_sectors / (hardsect_size / SECTOR_SIZE);
         geo.cylinders = (size & ~0x3f) >> 6;
         geo.heads = 4;
         geo.sectors = 16;
         geo.start = 4;
         if (copy_to_user((void __user *)arg, &geo, sizeof(geo))) {
            return -EFAULT;
         }
         return 0;
      };
   }

   return -ENOTTY;
}

//TODO: only works on page-aligned copies and doesn't check writes that are too long
static void pramdisk_submit_bio(struct bio* bio) {
   struct pramdisk_dev* dev = bio->bi_bdev->bd_disk->private_data;

   struct bio_vec bvec;
   struct bvec_iter it;

   sector_t cur_sector = bio->bi_iter.bi_sector;

   printk(KERN_INFO "doing a request\n");

   bio_for_each_segment(bvec, bio, it) {
      s64 bytes_to_copy = bvec.bv_len;

      s64 dev_offset = cur_sector * SECTOR_SIZE;
      void* dev_mem = dev->data + dev_offset;

      void* bvec_mem = kmap_local_page(bvec.bv_page);
      if (IS_ERR(bvec_mem)) {
         printk(KERN_ERR "Err mapping\n");
         bio_io_error(bio);
         return;
      }
      bvec_mem += bvec.bv_offset;

      // Copy sector
      if (bio_data_dir(bio) == WRITE) {
         memcpy(dev_mem, bvec_mem, bytes_to_copy);
      } else {
         memcpy(bvec_mem, dev_mem, bytes_to_copy);
      }

      cur_sector += bytes_to_copy >> SECTOR_SHIFT;

      kunmap_local(bvec_mem);
   }

   bio_endio(bio);
}

static struct block_device_operations pramdisk_ops = {
   .owner = THIS_MODULE,

   .open = pramdisk_open,
   .release = pramdisk_release,

   .submit_bio = pramdisk_submit_bio,

   .ioctl = pramdisk_ioctl,
};

//TODO error handle better
static int setup_device(struct pramdisk_dev* dev, int which) {
   int ret = 0;

   memset(dev, 0, sizeof(struct pramdisk_dev));
   dev->size_in_sectors = nsectors;
   printk(KERN_INFO "dev->size_in_sectors: %llu\n", dev->size_in_sectors);
   dev->data = vmalloc(hardsect_size * nsectors);
   if (IS_ERR(dev->data)) {
      printk(KERN_WARNING "vmalloc fail\n");
      return ret = -1;
   }

   dev->gd = blk_alloc_disk(NULL, NUMA_NO_NODE);
   if (IS_ERR(dev->gd)) {
      printk(KERN_WARNING "pramdisk: Failed to alloc disk\n");
      ret = -1;
      goto out_vfree;
   }

   // apparently that's auto-allocated by the block core?
   dev->gd->major = pramdisk_major;
   dev->gd->first_minor = which;
   dev->gd->minors = PRAMDISK_MINORS;
   dev->gd->fops = &pramdisk_ops;
   dev->gd->private_data = dev;
   dev->gd->flags |= GENHD_FL_NO_PART;
   snprintf(dev->gd->disk_name, DISK_NAME_LEN, "pramdisk_%c", which + '0');
   set_capacity(dev->gd, dev->size_in_sectors);

   ret = add_disk(dev->gd);
   if (ret) {
      printk(KERN_WARNING "pramdisk: Failed to add disk\n");
      ret = -1;
      goto failed_disk_add;
   }

   return ret;

failed_disk_add:
   put_disk(dev->gd); //or disk_release?
out_vfree:
   vfree(dev->data);
   return ret;
}

//TODO finish it
static void destroy_device(struct pramdisk_dev* dev) {
   if (dev->data) {
      vfree(dev->data);
   }
   if (dev->gd) {
      del_gendisk(dev->gd);
   }

   memset(dev, 0, sizeof(struct pramdisk_dev));
}

static int __init pramdisk_init(void) {
   printk(KERN_INFO "pramdisk init start\n");

   pramdisk_major = register_blkdev(pramdisk_major, "pramdisk");
   if (pramdisk_major == 0) {
      printk(KERN_ERR "Failed init of pramdisk\n");
      return -EBUSY;
   }
   printk(KERN_INFO "pramdisk major: %d\n", pramdisk_major);

   devs = kmalloc(ndevices * sizeof(struct pramdisk_dev), GFP_KERNEL);
   if (IS_ERR(devs)) {
      goto err;
   }
   memset(devs, 0, ndevices * sizeof(struct pramdisk_dev));

   for (int i = 0; i < ndevices; i++) {
      if (setup_device(devs + i, i) != 0) {
         printk(KERN_ERR "Failed to setup device '%d'\n", i);
         for (int j = 0; j < i; j++) {
            destroy_device(devs + j);
         }
         kfree(devs);
         goto err;
      } else {
         printk(KERN_INFO "Setup up device '%d' successfully\n", i);
      }
   }

   printk(KERN_INFO "pramdisk init complete\n");
   return 0;
err:
   printk(KERN_ERR "Failed pramdisk init\n");
   unregister_blkdev(pramdisk_major, "pramdisk");
   return -ENOMEM;
}

static void __exit pramdisk_exit(void) {
   for (int i = 0; i < ndevices; i++) {
      destroy_device(devs + i);
   }
   kfree(devs);
   unregister_blkdev(pramdisk_major, "pramdisk");
   printk(KERN_INFO "pramdisk exit\n");
}

module_init(pramdisk_init);
module_exit(pramdisk_exit);
MODULE_LICENSE("GPL");
